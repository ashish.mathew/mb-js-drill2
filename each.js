function each(elements, print)
{
    if (!Array.isArray(elements) || typeof(print) != "function")
    {
        throw Error("invalid arguments passed");
    }
    let n = elements.length;
    for (let i = 0; i < n; i++)
    {
        print(elements[i],i);
    }
}
function print(element,index)
{
    console.log(`${index}th element is ${element}`);
}
export {each,print};
