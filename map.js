import { m} from "./test/testMap.js"
function map(elements, multiply)
{
    let n = elements.length;
    let new_elements = [];
    for (let i = 0; i < n;i++)
    {
        new_elements.push(multiply(elements[i],m));
    }
    return new_elements;
}
function multiply(element,n)
{
    return element * n;
}
export {map,multiply};
