function filter(elements,is_even)
{
    if (!Array.isArray(elements) || typeof(is_even) != "function")
    {
        throw Error("invalid arguments passed");
    }
    let n = elements.length;
    let result = [];
    for (let i = 0; i < n; i++)
    {
        if(is_even(elements[i]))
            result.push(elements[i]);
    }
    return result;   
}
function is_even(element)
{
    if (element % 2 === 0)
        return true;
    return false;
}
export {filter,is_even};
