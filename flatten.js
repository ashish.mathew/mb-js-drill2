function flatten(elements)
{
    let flat_array = [];
    let n = elements.length;
    for (let i = 0; i < n; i++)
    {
        if (Array.isArray(elements[i]))
        {            
            flat_array = flat_array.concat(flatten(elements[i]));
        }
        else{
            flat_array.push(elements[i])
        }
    }
    return flat_array
}
export {flatten}