import { flatten } from "../flatten.js";

try {
    const nestedArray = [1, [2], [[3]], [[[4]]]];
    console.log(flatten(nestedArray));
} catch (error) {
    console.log(error);
}