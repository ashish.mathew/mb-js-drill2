function reduce(elements,accumulate,start)
{
    let n = elements.length;
    let i;
    if (start)
    {
        i = 0;
    }
    else{
        start = elements[0];
        i = 1;
    }
    for (i; i < n; i++)
    {
        start = accumulate(start,elements[i])
    }
    return start;
}
function accumulate(start,element)
{
    return start + element;
}
export {reduce,accumulate};