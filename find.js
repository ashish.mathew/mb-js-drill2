import { value } from "./test/testfind.js";
function find(elements,is_equal)
{
    let n = elements.length;
    for (let i = 0; i < n; i++)
    {
        if(is_equal(elements[i]))
        {
            return elements[i];
        }
    }
    return undefined;
}
function is_equal(element)
{
    if (element === value)
        return true;
    return false;

}
export {find,is_equal};